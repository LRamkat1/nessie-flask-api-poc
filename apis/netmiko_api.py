import json
import os
import netmiko
from .shell_api import ShellHelper

if os.path.isfile('./tokens.py'):
    from .tokens import Tokens

try:
    with open("/var/www/configure", "r") as file:
        environment = file.readlines()[1].strip()
except FileNotFoundError as e:
    environment = "None"
    print(e)

class NetmikoApi:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def connect_device(self, device):
        net_connect_2 = netmiko.ConnectHandler(device_type="cisco_ios", host=device, username=self.username, password=self.password)
        return net_connect_2


    def send_command(self, connection, command):
        output = connection.send_command(command)
        return output

    def disconnect_device(self, connection):
        connection.disconnect()

    def get_os(self, hostname):
        connection = self.connect_device(hostname)
        output = self.send_command(connection, "show version")
        self.disconnect_device(connection)
        return output

    def get_model(self):
        return "model"

    @staticmethod
    def get_net_creds():
        safe = "TC_0103220P_SVC-ACTS"
        cyberark_object = "OperatingSystem-gsm1900.org-SVC_PRD_APTX_CISCO"
        appID = "APP_GIT_103220_Ansible_CLI"

        if environment == "Local":
            net_usr = Tokens().netUser,
            net_pwd = Tokens().netPassword,
        else:
            shellhelper = ShellHelper()
            r = shellhelper.execute_shell_command('curl -X GET -E /etc/pki/ca-trust/source/anchors/ansttn05.unix.gsm1900.org.pem "https://ccp.cyberark.t-mobile.com/AIMWebservice/api/Accounts?AppID="' + appID + '"&object="' + cyberark_object + '"&Safe="' + safe, None)
            json_r = r.decode("utf-8").replace("", "")
           
            net_usr = 'SVC_PRD_APTX_CISCO'
            net_pwd = json.loads(json_r)['Content']

        ret_val = {
            'net_usr': net_usr,
            'net_pwd': net_pwd
        }

        return ret_val

    @staticmethod
    def get_platform(hostname):
        login_details = NetmikoApi.get_net_creds()
        net_usr = login_details["net_usr"]
        net_pwd = login_details["net_pwd"]

        netmiko_obj = NetmikoApi(net_usr, net_pwd)
        try: 
            connection = netmiko_obj.connect_device(hostname)
        except:
            return {"error": "Could not connect to device, please check hostnames and login credentials"}

        show_version = netmiko_obj.send_command(connection, "show version")
        if 'Cisco Nexus Operating System (NX-OS) Software' in show_version:
            platform = 'nxos'
        elif 'Cisco IOS Software' in show_version:
            platform = 'ios'
        elif 'Cisco IOS XR Software' in show_version:
            platform = 'iosxr'
        else:
            return {"error": hostname + "is not an IOS, NXOS, or IOS-XR device"}

        netmiko_obj.disconnect_device(connection)
        return platform


