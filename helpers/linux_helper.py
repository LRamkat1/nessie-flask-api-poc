from datetime import datetime

class LinuxHelper:
    time_start = str(datetime.strptime(str('2030-01-01') + ' 08:00:00', '%Y-%m-%d %H:%M:%S'))
    time_end = str(datetime.strptime(str('2030-01-01') + ' 08:00:00', '%Y-%m-%d %H:%M:%S'))

    def create_temp_directory(self, playbook_name):
        dir = playbook_name + str((datetime.now() - datetime.utcfromtimestamp(0)).total_seconds()) + '_temp'
        return dir