import json
import os
from flask import Flask, request, jsonify, send_file
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
from apis.netmiko_api import NetmikoApi
from helpers.linux_helper import LinuxHelper

app = Flask(__name__)
CORS(app)

path = "/var/www/descriptionupdate/"
playbook = "descriptionupdate"


@app.route("/preprocess", methods=["POST"])
def preprocess():
    # data = request.get_json()
    # device_list = data.get("device_list", None)
    # accepted_groups = data.get("accepted_groups", None)
    # all_vars = data.get("all_vars", None)

    device_list = json.loads(request.form.get("device_list"))
    accepted_groups = json.loads(request.form.get("accepted_groups"))
    # all_vars = json.loads(request.form.get("all_vars"))

    directory = LinuxHelper().create_temp_directory(playbook)

    if not device_list:
        return jsonify(message="Invalid request"), 400

   # for device in device_list:
   #     platform = NetmikoApi.get_platform(device.get("hostname"))
    #    if "error" in platform:
   #         return jsonify(platform["error"]), 500
   #     else:
    
    #        device["group"] = [platform] */


    return_data = {
        'accepted_groups': accepted_groups,
        'device_list': device_list,
        'all_vars': '{"ansible_connection": "local"}',
        'directory': directory
    }
    return jsonify(return_data=return_data), 201


@app.route("/sample", methods=["GET"])
def sample():
    path = '/var/www/description_update_bulk_sample_v2.xlsx'
    if os.path.exists(path):
        return send_file(path, attachment_filename="description_update_bulk_sample_v2.xlsx")
    else:
        return jsonify(message="could not find file"), 404


SWAGGER_URL = "/api/docs"
API_URL = "/static/swagger.json"

swagger_ui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        "app_name": "Flask Netmiko Api"
    }
)

app.register_blueprint(swagger_ui_blueprint)

if __name__ == '__main__':
    app.run(debug=True)
