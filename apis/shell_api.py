import subprocess

class ShellHelper:
    def __init__(self):
        pass

    def execute_shell_command(self, cmd, work_dir):
        """Execute a command in the subprocess, wait until it is finished"""
        pipe = subprocess.Popen(cmd, shell=True, cwd=work_dir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (out, err) = pipe.communicate()
        pipe.wait()
        return out
